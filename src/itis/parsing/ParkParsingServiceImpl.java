package itis.parsing;

import itis.parsing.annotations.MaxLength;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException, IOException {
        BufferedReader parkReader = new BufferedReader(new FileReader(parkDatafilePath));
        String line1=null;
        ArrayList<String> strings = new ArrayList<>();
        while ((line1 = parkReader.readLine()) != null) {
            String[] atributes = line1.split(":");
            try {
                atributes[1].replace(String.valueOf(atributes[1].charAt(0)),"");
                atributes[1].replace(String.valueOf(atributes[1].charAt(atributes[1].length()-1)),"");
                strings.add(atributes[1]);
            } catch (ArrayIndexOutOfBoundsException e) {

            }
        }
        for (int i = 0; i < strings.size(); i++) {
            System.out.println(strings.get(i));
        }

            try {
                Park myClass = null;

                    Class park = Class.forName(Park.class.getName());
                    park.getDeclaredConstructor().setAccessible(true);
                    Field[] fields = new Field[park.getDeclaredFields().length];
                    fields=park.getDeclaredFields();
                    for (int i = 0; i < fields.length; i++) {
                        fields[i].setAccessible(true);
                        System.out.println(fields[i].getAnnotation(MaxLength.class));
                        try {


                            fields[i].set(park, strings.get(i));
                        }catch(IllegalArgumentException e){
                            System.out.println("wat");
                        }
                    }
                    myClass = (Park) park.newInstance();
                    System.out.println(myClass);
                    System.out.println(park);
                    return myClass;
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException  | NoSuchMethodException | ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }



        return null;
    }

    public static void main(String[] args) throws IOException {
        new ParkParsingServiceImpl().parseParkData("C:\\Users\\admin\\IdeaProjects\\src\\park-quiz\\src\\itis\\parsing\\resources\\parkdata");
    }
}
